package com.example.miniproject.exception;

public class InvalidFieldCategoryException extends RuntimeException{
    public InvalidFieldCategoryException(String message) {
        super(message);
    }
}
