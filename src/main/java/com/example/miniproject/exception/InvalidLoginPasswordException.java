package com.example.miniproject.exception;

public class InvalidLoginPasswordException extends RuntimeException{
    public InvalidLoginPasswordException() {
    }

    public InvalidLoginPasswordException(String message) {
        super(message);
    }
}
