package com.example.miniproject.exception;

public class UserEmailIsNotFoundException extends RuntimeException{
    public UserEmailIsNotFoundException() {
    }

    public UserEmailIsNotFoundException(String message) {
        super(message);
    }
}
