package com.example.miniproject.exception;

public class EmptyFieldEmailException extends RuntimeException{
    public EmptyFieldEmailException() {
    }

    public EmptyFieldEmailException(String message) {
        super(message);
    }
}
