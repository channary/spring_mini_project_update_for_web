package com.example.miniproject.exception;

public class EmptyFieldCategoryException extends RuntimeException{
    public EmptyFieldCategoryException(String message) {
        super(message);
    }
}
