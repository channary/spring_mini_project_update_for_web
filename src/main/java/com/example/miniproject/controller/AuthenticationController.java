package com.example.miniproject.controller;

import com.example.miniproject.exception.*;
import com.example.miniproject.model.entity.UserDTO;
import com.example.miniproject.model.request.AuthenicationRequest;
import com.example.miniproject.model.response.AuthenticationResponse;
import com.example.miniproject.model.response.UserResponse;
import com.example.miniproject.securityConfig.PasswordConfig;
import com.example.miniproject.service.AuthenticationService;
import com.example.miniproject.service.serviceImp.UserServiceImp;
import lombok.RequiredArgsConstructor;
import org.bouncycastle.openssl.PasswordException;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {
    private final AuthenticationService authenticationService;
    List<AuthenicationRequest> users = new ArrayList<>();
    private final UserServiceImp userServiceImp;
    private final ModelMapper modelMapper;
    private final PasswordConfig passwordEncoder;

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(
            @RequestBody AuthenicationRequest authenticationRequest
    ){
        String regexpEmail = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";

        if(!authenticationRequest.getEmail().matches(regexpEmail))
            throw new InvalidFieldEmailExpcetion();
        else if (authenticationRequest.getEmail().isEmpty())
            throw new EmptyFieldEmailException();
        else if (userServiceImp.getUserByEmail(authenticationRequest.getEmail())==null) {
            throw new UserEmailIsNotFoundException();
        } else {
            if(authenticationRequest.getPassword().isEmpty())
                throw new EmptyFieldPasswordException();
             else {
                     var userDetial = authenticationService.authenticate(authenticationRequest);
                     System.out.println("USERDETIAL : " + userDetial);
                     UserResponse<?> response = UserResponse.<AuthenticationResponse>builder()
                             .payload(userDetial)
                             .date(new Timestamp(System.currentTimeMillis()))
                             .success(true)
                             .build();
                     return ResponseEntity.ok(response);
            }
        }
    }


    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AuthenicationRequest authenicationRequest) {
        String regexpEmail = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}";
        String regexPassword = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$";
        System.out.println(userServiceImp.getUserByEmail(authenicationRequest.getEmail()));
        if(authenicationRequest.getEmail().matches(regexpEmail) )
        {
            if (userServiceImp.getUserByEmail(authenicationRequest.getEmail())!=null)
            {
                throw new UserEmailIsTakenException();
            }else
            {
                if(authenicationRequest.getPassword().isEmpty())
                    throw new EmptyFieldPasswordException();
                else
                {

                    if (authenicationRequest.getPassword().matches(regexPassword))
                    {

                        authenicationRequest.setPassword(passwordEncoder.passwordEncoder().encode(authenicationRequest.getPassword()));
                        Integer storeUserId = userServiceImp.addNewUser(authenicationRequest);

                        AuthenicationRequest user = new AuthenicationRequest(authenicationRequest.getEmail(), authenicationRequest.getPassword());
                        users.add(user);

                        UserDTO userDTO = modelMapper.map(user,UserDTO.class);
                        userDTO.setUserId(storeUserId);

                        UserResponse<?> response = UserResponse.<UserDTO>builder()
                                .payload(userDTO)
                                .date(new Timestamp(System.currentTimeMillis()))
                                .success(true)
                                .build();

                        return ResponseEntity.ok(response);
                    }else
                        throw new InvalidPasswordException();
                }
            }
        }else if (authenicationRequest.getEmail().isEmpty())
        {
            throw new EmptyFieldEmailException();
        }
        else {
            throw new InvalidFieldEmailExpcetion();

        }

    }


}
