package com.example.miniproject.repository;
import com.example.miniproject.model.request.AuthenicationRequest;
import com.example.miniproject.model.entity.UserInfo;
import org.apache.ibatis.annotations.*;

@Mapper
public interface UserRepository {

    @Select("SELECT * FROM user_tb WHERE email = #{email}")
    UserInfo findByEmail(@Param("email") String email);

    @Select("INSERT INTO user_tb(email, password) " +
            "VALUES (#{request.email},#{request.password}) RETURNING id")
    Integer addNewUser(@Param("request") AuthenicationRequest authenicationRequest);

    @Select("""
            SELECT * FROM user_tb WHERE id = #{userId}
            """)
    UserInfo getUserById(Integer userId);

    @Select("""
            SELECT * FROM user_tb WHERE email = #{userEmail}
            """)
    Integer getUserByEmail(String userEmail);

    @Select("""
            SELECT password FROM user_tb WHERE email = #{userEmail}
            """)
    String getUserPasswordByEmail(String UserEmail);

//    @Select("INSERT INTO user_role_tb (user_id, role_id) " +
//            "VALUES (#{userId}, #{roleId})")
//    Integer addRoleUserByUserId(@Param("userId") Integer userId, @Param("roleId") Integer roleId);



}
