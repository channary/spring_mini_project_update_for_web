package com.example.miniproject.repository;

import com.example.miniproject.model.entity.Category;
import com.example.miniproject.model.entity.Task;
import com.example.miniproject.model.request.TaskRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface TaskRepository {


    @Select("SELECT * FROM task_tb WHERE id = #{taskId} AND user_id = #{userId}")
    @Results(
            id = "taskMap", value = {
            @Result(property = "taskId", column = "id"),
            @Result(property = "taskName", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "taskName", column = "name"),
            @Result(property = "categoryId", column = "category_id")
    }
    )
    Task getTaskByIdCurrentUser(Integer taskId, Integer userId);


    @Select("""
            INSERT INTO task_tb (name,description,status,category_id,date,user_id)
            VALUES(#{request.taskName},#{request.description},#{request.status},#{request.categoryId},#{request.date},#{userId})
            RETURNING id
            """)
    Integer insertTask(@Param("request") TaskRequest taskRequest, Integer userId);

    @Delete("""
            DELETE FROM task_tb where id = #{id} AND user_id = #{userId}
            """)
    Boolean deleteTaskById(@Param("id") Integer taskId,Integer userId);


    @Select("""
            SELECT * FROM task_tb WHERE user_id = #{id} ORDER BY id 
            LIMIT #{size} OFFSET #{size} * (#{page} - 1) 
            """)
    @ResultMap("taskMap")
    List<Task> findAllTaskCurrentUser(@Param("id") Integer userId, Integer page ,Integer size);

    @Select("""
            SELECT * FROM task_tb WHERE user_id = #{id} ORDER BY id DESC 
            LIMIT #{size} OFFSET #{size} * (#{page} - 1) 
            """)
    @ResultMap("taskMap")
    List<Task> findAllTaskCurrentUserDesc(@Param("id") Integer userId, Integer page ,Integer size);


    @Select("SELECT * FROM task_tb WHERE user_id = #{userId} AND status = #{status}")
    @ResultMap("taskMap")
    List<Task> getTaskByStatusCurrentUser( Integer userId,String status);

    @Results(
            id = "getMapping", value = {
                    @Result(property = "taskId", column = "id"),
                    @Result(property = "taskName", column = "name"),
                    @Result(property = "description", column = "description"),
                    @Result(property = "date", column = "date"),
                    @Result(property = "userId", column = "user_id"),
                    @Result(property = "categoryId", column = "category_id")

    })
    @Select("""
            SELECT * FROM task_tb ORDER BY id
            LIMIT #{size} OFFSET #{size} * (#{page} - 1)
            """)
    List<Task> getAllTask(Integer page, Integer size);

    @Select("""
            SELECT * FROM task_tb ORDER BY id DESC
                              LIMIT #{size} OFFSET #{size} * (#{page} -1 )
            """)
    @ResultMap("taskMap")
    List<Task> getAllTaskOrderDesc(Integer page, Integer size);


    @Select("""
            SELECT * FROM task_tb WHERE id = #{taskId}
            """)
    @ResultMap("getMapping")
    Task getTaskById(Integer taskId);


    @Update("""
            UPDATE task_tb
            SET name = #{task.taskName}, description = #{task.description}, date = #{task.date}, status= #{task.status},
            user_id = #{task.userId} , category_id = #{task.categoryId}
            WHERE id = #{taskId}    
            RETURNING id
            """)
    @ResultMap("getMapping")
    Integer updateTask(Integer taskId, @Param("task") TaskRequest taskRequest);

}
