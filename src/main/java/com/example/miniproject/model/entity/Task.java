package com.example.miniproject.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Task {
    private Integer taskId;
    private String taskName;
    private String description;
    private Timestamp date;
    private Integer userId;
    private String status;
    private Integer categoryId;
}
