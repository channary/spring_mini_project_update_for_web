package com.example.miniproject.model.request;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthenicationRequest {
    private String email;
    private String password;
}
